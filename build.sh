#!/usr/bin/env bash

docker build -f Dockerfile -t docker-registry.uzabase.com/speeda/langhub-ui:$1 .
docker push docker-registry.uzabase.com/speeda/langhub-ui:$1