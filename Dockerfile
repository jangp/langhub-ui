FROM nginx:1.17.8-alpine
# Copy needed files
COPY nginx.config /etc/nginx/conf.d/default.conf
COPY dist /usr/share/nginx/html