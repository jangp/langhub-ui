# LangHub UI


## Developing
Install [yarn](https://classic.yarnpkg.com/en/docs/install/) \
Install [elm-format globally](https://github.com/avh4/elm-format#installation-) for auto code format 

Mock server
```bash
$ json-server --watch mocksv/db.json --port 4000
```

Start
```bash
$ yarn local
```

## Production

Build production assets (js and css together) to `/dist` and build docker image

```bash
$ yarn prod
```

## Testing

Install [elm-test globally](https://github.com/elm-community/elm-test#running-tests-locally)

```bash
$ yarn test
```

## Elm-analyse

Install [elm-analyse globally](https://github.com/stil4m/elm-analyse)

Run analysis:
```bash
$ yarn analyse
```