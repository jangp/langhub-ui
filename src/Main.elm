module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Json exposing (Decoder, field, int, map2, string)
import Task exposing (Task)



-- ---------------------------
-- MODEL
-- ---------------------------


type alias Config =
    { host : String
    }


type alias Model =
    { searchText : String
    , result : List Translation
    , serverMessage : String
    , host : String
    }


type alias Translation =
    { id : Int
    , translated : String
    }


init : Config -> ( Model, Cmd Msg )
init config =
    ( { searchText = ""
      , result = []
      , serverMessage = ""
      , host = config.host
      }
    , Cmd.none
    )



-- ---------------------------
-- UPDATE
-- ---------------------------


type Msg
    = Search
    | Change String
    | OnServerResponse (Result Http.Error (List Translation))
    | Update


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        Search ->
            ( model
            , Task.attempt OnServerResponse (getTranslation model.host model.searchText)
            )

        Change text ->
            ( { model | searchText = text }, Cmd.none )

        OnServerResponse res ->
            case res of
                Ok r ->
                    ( { model | result = r }, Cmd.none )

                Err _ ->
                    ( { model | serverMessage = "Error!" }, Cmd.none )

        Update ->
            ( model, Cmd.none )


handleJsonResponse : Decoder a -> Http.Response String -> Result Http.Error a
handleJsonResponse decoder response =
    case response of
        Http.BadUrl_ url ->
            Err (Http.BadUrl url)

        Http.Timeout_ ->
            Err Http.Timeout

        Http.BadStatus_ { statusCode } _ ->
            Err (Http.BadStatus statusCode)

        Http.NetworkError_ ->
            Err Http.NetworkError

        Http.GoodStatus_ _ body ->
            case Json.decodeString decoder body of
                Err _ ->
                    Err (Http.BadBody body)

                Ok result ->
                    Ok result


getTranslation : String -> String -> Task Http.Error (List Translation)
getTranslation host text =
    Http.task
        { method = "GET"
        , headers = []
        , url = host ++ "/translations"
        , body = Http.emptyBody
        , resolver = Http.stringResolver <| handleJsonResponse <| Json.list translationDecoder
        , timeout = Nothing
        }


translationDecoder : Decoder Translation
translationDecoder =
    map2 Translation
        (field "id" int)
        (field "translated" string)


displayResult : List Translation -> List (Html msg)
displayResult list =
    List.map toParagraph list


toParagraph : Translation -> Html msg
toParagraph t =
    div [ class "pure-g box" ]
        [ div [ class "pure-u-1-2" ]
            [ p
                []
                [ text t.translated ]
            ]
        , div [ class "pure-u-1-2 l-box" ]
            [ textarea
                [ style "width" "100%", style "height" "100%" ]
                [ text t.translated ]
            ]
        ]



-- ---------------------------
-- VIEW
-- ---------------------------


view : Model -> Html Msg
view model =
    div [ class "container content" ]
        [ header []
            [ h1 [] [ text "Language Hub Editor" ]
            ]
        , div
            [ class "pure-form"
            ]
            [ input [ class "pure-input-rounded", placeholder "Any Phrase", value model.searchText, onInput Change ] []
            , button [ class "pure-button pure-button-active l-box", onClick Search ] [ text "Search" ]
            , button [ class "pure-button pure-button-primary", onClick Update, disabled True ] [ text "Update" ]
            ]
        , div []
            (displayResult model.result)
        , div []
            [ text model.serverMessage ]
        , div [] [ text (String.reverse model.searchText) ]
        ]



-- ---------------------------
-- MAIN
-- ---------------------------


main : Program Config Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view =
            \m ->
                { title = "Language Hub"
                , body = [ view m ]
                }
        , subscriptions = \_ -> Sub.none
        }
