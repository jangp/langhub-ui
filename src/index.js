import {Elm} from './Main.elm'

Elm.Main.init({
    flags: {
        host: process.env.LANGHUB_HOST
    }
});
